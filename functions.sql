/*--------- FUNCTIONS: ----------*/
-- add_artist
-- Function that adds an artist to the artist table if they do not already exist
CREATE OR REPLACE FUNCTION add_artist(
	name_in TEXT,
	nationality_in TEXT,
	rating_in INTEGER
)
RETURNS VOID
AS $$
DECLARE
	rec record;
BEGIN
	SELECT * INTO rec FROM artists
	WHERE name = name_in;
	IF NOT FOUND THEN
		INSERT INTO artists (name, nationality, rating)
		VALUES (name_in, nationality_in, rating_in);
	ELSE
		RAISE EXCEPTION 'Artist % alreadty exists', name_in;
	END IF;
END;
$$ LANGUAGE plpgsql;

-- add_album
-- Function that adds a new album to the albums table if they do not already exist
CREATE OR REPLACE FUNCTION add_album(
    title_in TEXT, 
    artist_id_in INTEGER, 
    release_date_in DATE, 
	duration_in INTERVAL, 
    album_rating_in INTEGER
)
RETURNS VOID
AS $$
DECLARE
	rec record;
BEGIN
	SELECT * INTO rec FROM albums
	WHERE title = title_in;
	IF NOT FOUND THEN
		INSERT INTO albums(title, artist_id, release_date, duration, album_rating, average_album_rating)
		VALUES	(title_in, artist_id_in, release_date_in, duration_in, album_rating_in, 0);
	ELSE
		RAISE EXCEPTION 'Album "%" already exists', title_in;
	END IF;
END;
$$ LANGUAGE plpgsql;

-- update_artist_rating
-- Function that updates artist rating
CREATE OR REPLACE FUNCTION update_artist_rating(artist_id_in INTEGER, new_rating_in INTEGER)
RETURNS void AS $$
BEGIN
	UPDATE artists SET artist_rating = new_rating_in WHERE artist_id = artist_id_in;
END;
$$ LANGUAGE plpgsql;

-- update_album_rating
-- Function that updates album rating
CREATE OR REPLACE FUNCTION update_album_rating(album_id_in INTEGER, new_rating_in INTEGER)
RETURNS void AS $$
BEGIN
	UPDATE albums SET album_rating = new_rating_in WHERE album_id = album_id_in;
END;
$$ LANGUAGE plpgsql;

-- update_song_rating
-- Function that updates song rating
CREATE OR REPLACE FUNCTION update_song_rating(song_id_in INTEGER, new_rating_in INTEGER)
RETURNS void AS $$
BEGIN
	UPDATE songs SET song_rating = new_rating_in WHERE song_id = song_id_in;
END;
$$ LANGUAGE plpgsql;

-- get_artist_songs
-- Function that returns a table of an artists songs based on their artist id
DROP FUNCTION get_artist_songs(artist_id_in INTEGER);
CREATE OR REPLACE FUNCTION get_artist_songs(artist_id_in INTEGER)
RETURNS TABLE(song_title TEXT, album_title TEXT, duration INTERVAL, song_rating INTEGER)
AS $$
DECLARE
    song_record RECORD;
BEGIN
    FOR song_record IN 
        SELECT s.title AS song_title, a.title AS album_title, s.duration, s.song_rating
        FROM songs s
        JOIN albums a ON s.album_id = a.album_id
        WHERE s.artist_id = artist_id_in
    LOOP
        song_title = song_record.song_title;
        album_title = song_record.album_title;
        duration = song_record.duration;
		song_rating = song_record.song_rating;
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$$ LANGUAGE plpgsql;

-- count_songs_for_artist
-- Function that returns the number of songs an artist has
CREATE OR REPLACE FUNCTION count_songs_for_artist(artist_id_in INTEGER)
RETURNS INTEGER AS $$
DECLARE
	song_count INTEGER;
BEGIN
	SELECT COUNT(*) INTO song_count
	FROM songs s
	JOIN artists ar ON s.artist_id = ar.artist_id
	WHERE ar.artist_id = artist_id_in;
	RETURN song_count;
END;
$$ LANGUAGE plpgsql;

-- SELECT * FROM get_artist_songs(1);

/*--------- TRIGGERS: ----------*/
-- Trigger that automatically sets song rating to zero if null
CREATE OR REPLACE FUNCTION set_song_rating()
RETURNS TRIGGER AS $$
BEGIN
	IF NEW.song_rating IS NULL THEN
		NEW.song_rating = 0;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_song_rating_trigger
BEFORE INSERT ON songs
FOR EACH ROW
EXECUTE PROCEDURE set_song_rating();

-- Trigger that updates average rating for an album whenever a songs rating is updated
CREATE OR REPLACE FUNCTION update_album_average()
RETURNS TRIGGER AS $$
DECLARE
	average_rating INTEGER;
BEGIN
	IF NEW.album_id IS NOT NULL THEN
		SELECT AVG(song_rating)
		INTO average_rating
		FROM songs
		WHERE album_id = NEW.album_id;
	
		UPDATE albums
		SET average_album_rating = average_rating
		WHERE album_id = NEW.album_id;
	END IF;
	
	RAISE NOTICE 'Album average rating updated.';
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_album_average_trigger
AFTER UPDATE ON songs
FOR EACH ROW
EXECUTE PROCEDURE update_album_average();

-- Trigger that updates artist rating when album rating is updated
CREATE OR REPLACE FUNCTION update_artist_rating()
RETURNS TRIGGER AS $$
DECLARE
	average_album_rating INTEGER;
BEGIN
	IF NEW.album_id IS NOT NULL THEN
		SELECT AVG(album_rating)
		INTO average_album_rating
		FROM albums
		WHERE artist_id = NEW.artist_id;
		
		UPDATE artists
		SET artist_rating = average_album_rating
		WHERE artist_id = NEW.artist_id;
	END IF;
	
	RAISE NOTICE 'Artist rating updated using average album rating.';
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_artist_rating_trigger
AFTER UPDATE ON albums
FOR EACH ROW
EXECUTE PROCEDURE update_artist_rating();


/*----------- VIEWS: -----------*/
-- All music data table
DROP VIEW IF EXISTS all_music_data;
CREATE OR REPLACE VIEW all_music_data AS 
SELECT ar.artist_id, ar.name, ar.nationality, ar.artist_rating, 
	al.album_id, al.title as album_title, al.release_date, 
	al.duration as album_duration, al.album_rating, al.average_album_rating, s.song_id, 
	s.title as song_title, s.duration as song_duration, s.song_rating
	FROM artists ar	FULL JOIN albums al USING(artist_id) 
	FULL JOIN songs s USING(album_id)
	ORDER BY artist_id, album_id, song_id;