-- VIEWS:
-- All music data view
SELECT * FROM all_music_data;
SELECT * FROM artists;
SELECT * FROM albums;
SELECT * FROM songs;

-- GET FUNCTIONS:
-- Returns a table of all of an artists songs
SELECT * FROM get_artist_songs((SELECT artist_id FROM artists WHERE name = 'Radiohead'));

-- Returns a count of all of an artists songs
SELECT * FROM count_songs_for_artist((SELECT artist_id FROM artists WHERE name ='Tame Impala'));

-- ADD FUNCTIONS:
-- Add artist
SELECT * FROM add_artist('name', 'nationality', 99);

-- Add album
SELECT * FROM add_album('Currents B-Sides & Remixes', (SELECT artist_id FROM artists WHERE name = 'Tame Impala'), '2017-11-16', '0:28:05', 92);
-- Add song

-- UPDATE FUNCTIONS:
-- Update an artist rating
SELECT * FROM update_artist_rating((SELECT artist_id FROM artists WHERE name = ''), 99);
SELECT * FROM artists WHERE name = '';

-- Update an album rating
SELECT * FROM update_album_rating((SELECT album_id FROM albums WHERE title = 'Hi This Is Flume'), 99);
SELECT * FROM albums WHERE title = '';

-- Update a song rating
SELECT * FROM update_song_rating((SELECT song_id FROM songs WHERE title = 'The Bends'), 86);
SELECT * FROM songs WHERE title = '';

